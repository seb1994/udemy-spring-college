package com.formacion.microservicios.app.usuarios.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.microservicios.app.usuarios.clients.CursoFeignClient;
import com.formacion.microservicios.app.usuarios.model.repository.AlumnoRepository;
import com.formacion.microservicios.commons.services.CommonSrvImpl;
import com.formacion.microservicios.commons.usuarios.model.entity.Alumno;

@Service
public class AlumnoSrvImpl extends CommonSrvImpl<Alumno, AlumnoRepository> implements AlumnoSrv {

	@Autowired
	private CursoFeignClient cursoClient;
	
	@Override
	@Transactional(readOnly = true)
	public List<Alumno> findByNombreOrApellido(String term) {
		return repository.findByNombreOrApellido(term);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Alumno> findAllById(List<Long> ids) {
		return (List<Alumno>) repository.findAllById(ids);
	}

	@Override
	public void eliminarCursoAlumnoById(Long id) {
		cursoClient.eliminarCursoAlumnoById(id);
	}
	
	@Override
	@Transactional
	public void delete(Long id) {
		super.delete(id);
		this.eliminarCursoAlumnoById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<Alumno> findAll() {		
		return repository.findAllByOrderByIdAsc();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Alumno> findAll(Pageable pageable) {
		return repository.findAllByOrderByIdAsc(pageable);
	}
	
	

}
