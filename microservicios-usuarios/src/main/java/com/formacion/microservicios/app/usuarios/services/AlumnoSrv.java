package com.formacion.microservicios.app.usuarios.services;

import java.util.List;

import com.formacion.microservicios.commons.services.CommonSrv;
import com.formacion.microservicios.commons.usuarios.model.entity.Alumno;

public interface AlumnoSrv extends CommonSrv<Alumno> {
	
	public List<Alumno> findByNombreOrApellido (String term);
	
	public List<Alumno> findAllById(List <Long> ids);
	
	public void eliminarCursoAlumnoById(Long id);
	
}
