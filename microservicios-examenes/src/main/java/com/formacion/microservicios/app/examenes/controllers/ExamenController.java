package com.formacion.microservicios.app.examenes.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.microservicios.app.examenes.services.ExamenSrv;
import com.formacion.microservicios.commons.controllers.CommonController;
import com.formacion.microservicios.commons.examenes.model.entity.Examen;

@RestController
public class ExamenController extends CommonController<Examen, ExamenSrv>  {
	
	@GetMapping("/respondidos-por-preguntas")
	public ResponseEntity<?> obtenerExamenesIdsPorPreguntasIdRespondidas(@RequestParam List<Long> preguntaIds){
		return ResponseEntity.ok(service.findExamenesIdsConRespuestasByPreguntaIds(preguntaIds));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> editarExamen(@Valid @RequestBody Examen examen, BindingResult result, @PathVariable Long id){
		
		if(result.hasErrors()) {
			return this.validar(result);
		}
		
		Optional<Examen> opt = service.findById(id);
		
		if(!opt.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Examen examenDB = opt.get();
		examenDB.setNombre(examen.getNombre());
		
		/*
		List<Pregunta> eliminadas = new ArrayList<>();
		
		examenDB.getPreguntas().forEach(pdb -> {
			if(!examen.getPreguntas().contains(pdb)) {
				eliminadas.add(pdb);
			}
		});
		
		eliminadas.forEach(p -> {
			examenDB.removePregunta(p);
		});
		
		eliminadas.forEach(examenDB :: removePregunta);
		
		*/
		
		//Crea un flujo Stream(lista) lo filtra con la condicion y luego lo recorre para eliminar las preguntas
		//Código equivalente a los comentarios de arriba
		examenDB.getPreguntas().stream().filter(pdb -> !examen.getPreguntas().contains(pdb)).forEach(examenDB :: removePregunta);
		
		examenDB.setPreguntas(examen.getPreguntas());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(examenDB));
	}
	
	@GetMapping("/filtrar/{term}")
	public ResponseEntity<?> filtrar(@PathVariable String term){
		return ResponseEntity.ok(service.findByNombreExamen(term));
	}
	
	@GetMapping("/asignaturas")
	public ResponseEntity<?> listarAsignaturas(){
		return ResponseEntity.ok(service.findAllAsignaturas());
	}

}
