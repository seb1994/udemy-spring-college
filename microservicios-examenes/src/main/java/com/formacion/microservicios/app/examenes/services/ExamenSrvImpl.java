package com.formacion.microservicios.app.examenes.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.microservicios.app.examenes.model.repository.AsignaturaRepository;
import com.formacion.microservicios.app.examenes.model.repository.ExamenRepository;
import com.formacion.microservicios.commons.examenes.model.entity.Asignatura;
import com.formacion.microservicios.commons.examenes.model.entity.Examen;
import com.formacion.microservicios.commons.services.CommonSrvImpl;

@Service
public class ExamenSrvImpl extends CommonSrvImpl<Examen, ExamenRepository> implements ExamenSrv {

	@Autowired
	private AsignaturaRepository asignaturaRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Examen> findByNombreExamen(String term) {
		return repository.findByNombreExamen(term);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Asignatura> findAllAsignaturas() {
		return (List<Asignatura>) asignaturaRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Long> findExamenesIdsConRespuestasByPreguntaIds(List<Long> preguntaIds) {
		return repository.findExamenesIdsConRespuestasByPreguntaIds(preguntaIds);
	}
	
	

}
