package com.formacion.microservicios.app.examenes.model.repository;

import org.springframework.data.repository.CrudRepository;

import com.formacion.microservicios.commons.examenes.model.entity.Asignatura;

public interface AsignaturaRepository extends CrudRepository<Asignatura, Long> {

}
