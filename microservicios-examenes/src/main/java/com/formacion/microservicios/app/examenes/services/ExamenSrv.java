package com.formacion.microservicios.app.examenes.services;

import java.util.List;

import com.formacion.microservicios.commons.examenes.model.entity.Asignatura;
import com.formacion.microservicios.commons.examenes.model.entity.Examen;
import com.formacion.microservicios.commons.services.CommonSrv;

public interface ExamenSrv extends CommonSrv<Examen>  {
	
	public List<Examen> findByNombreExamen (String term);
	
	public List<Asignatura> findAllAsignaturas();

	public List<Long> findExamenesIdsConRespuestasByPreguntaIds(List<Long> preguntaIds);
}
