package com.formacion.microservicios.app.cursos.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.microservicios.app.cursos.model.entity.Curso;
import com.formacion.microservicios.app.cursos.model.entity.CursoAlumno;
import com.formacion.microservicios.app.cursos.services.CursoSrv;
import com.formacion.microservicios.commons.controllers.CommonController;
import com.formacion.microservicios.commons.examenes.model.entity.Examen;
import com.formacion.microservicios.commons.usuarios.model.entity.Alumno;

@RestController
public class CursoController extends CommonController<Curso, CursoSrv> {

	@Value("${config.balanceador.test}")
	private String balanceadorTest;

	@DeleteMapping("/eliminar-alumno-curso/{id}")
	public ResponseEntity<?> eliminarCursoAlumnoById(@PathVariable Long id) {
		service.eliminarCursoAlumnoById(id);

		return ResponseEntity.noContent().build();
	}

	@GetMapping
	@Override
	public ResponseEntity<?> listar() {
		List<Curso> cursos = ((List<Curso>) service.findAll()).stream().map(c -> {
			c.getCursoAlumnos().forEach(ca -> {
				Alumno alumno = new Alumno();
				alumno.setId(ca.getAlumnoId());

				c.addAlumno(alumno);
			});
			return c;
		}).collect(Collectors.toList());

		return ResponseEntity.ok().body(cursos);
	}

	@GetMapping("/pagina")
	@Override
	public ResponseEntity<?> listar(Pageable pageable) {
		Page<Curso> cursos = service.findAll(pageable).map(curso -> {
			curso.getCursoAlumnos().forEach(ca -> {
				Alumno alumno = new Alumno();
				alumno.setId(ca.getAlumnoId());

				curso.addAlumno(alumno);
			});
			return curso;
		});

		return ResponseEntity.ok().body(cursos);
	}

	@GetMapping("/{id}")
	@Override
	public ResponseEntity<?> ver(@PathVariable Long id) {
		Optional<Curso> opt = service.findById(id);

		if (opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Curso curso = opt.get();

		if (!curso.getCursoAlumnos().isEmpty()) {

			List<Long> ids = curso.getCursoAlumnos().stream().map(ca -> ca.getAlumnoId()).collect(Collectors.toList());

			List<Alumno> alumnos = service.obtenerAlumnosPorCurso(ids);

			curso.setAlumnos(alumnos);
		}

		return ResponseEntity.ok().body(curso);
	}

	@GetMapping("/balanceador-test")
	public ResponseEntity<?> balanceadorTest() {
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("balanceador", balanceadorTest);
		response.put("cursos", service.findAll());

		return ResponseEntity.ok(response);
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> editarCurso(@Valid @RequestBody Curso curso, BindingResult result, @PathVariable Long id) {

		if (result.hasErrors()) {
			return this.validar(result);
		}

		Optional<Curso> opt = this.service.findById(id);

		if (opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Curso cursoDB = opt.get();
		cursoDB.setNombre(curso.getNombre());

		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	}

	@PutMapping("/{id}/asignar-alumnos")
	public ResponseEntity<?> asignarAlumnos(@RequestBody List<Alumno> alumnos, @PathVariable Long id) {
		Optional<Curso> opt = this.service.findById(id);

		if (opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Curso cursoDB = opt.get();

		alumnos.forEach(a -> {
			CursoAlumno cursoAlumno = new CursoAlumno();
			cursoAlumno.setAlumnoId(a.getId());
			cursoAlumno.setCurso(cursoDB);

			cursoDB.addCursoAlumno(cursoAlumno);
		});

		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	}

	@PutMapping("/{id}/eliminar-alumno")
	public ResponseEntity<?> eliminarAlumno(@RequestBody Alumno alumno, @PathVariable Long id) {
		Optional<Curso> opt = this.service.findById(id);

		if (opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Curso cursoDB = opt.get();

		CursoAlumno cursoAlumno = new CursoAlumno();
		cursoAlumno.setAlumnoId(alumno.getId());

		cursoDB.removeCursoAlumno(cursoAlumno);

		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	}

	@GetMapping("/alumno/{id}")
	public ResponseEntity<?> buscarPorAlumnoId(@PathVariable Long id) {
		Curso cursoDB = service.findCursoByAlumnoId(id);

		if (cursoDB != null) {
			List<Long> examenesId = (List<Long>) service.obtenerExamenesRespondidosPorAlumno(id);

			if(examenesId != null && !examenesId.isEmpty()) {
				List<Examen> examenes = cursoDB.getExamenes().stream().map(examen -> {
					if (examenesId.contains(examen.getId())) {
						examen.setRespondido(true);
					}
					return examen;
				}).collect(Collectors.toList());
				
				cursoDB.setExamenes(examenes);
			}
		}

		return ResponseEntity.ok(cursoDB);
	}

	@PutMapping("/{id}/asignar-examenes")
	public ResponseEntity<?> asignarExamenes(@RequestBody List<Examen> examenes, @PathVariable Long id) {
		Optional<Curso> opt = this.service.findById(id);

		if (opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Curso cursoDB = opt.get();

		examenes.forEach(e -> {
			cursoDB.addExamen(e);
		});

		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	}

	@PutMapping("/{id}/eliminar-examen")
	public ResponseEntity<?> eliminarExamen(@RequestBody Examen examen, @PathVariable Long id) {
		Optional<Curso> opt = this.service.findById(id);

		if (opt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		Curso cursoDB = opt.get();

		cursoDB.removeExamen(examen);

		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(cursoDB));
	}

}
