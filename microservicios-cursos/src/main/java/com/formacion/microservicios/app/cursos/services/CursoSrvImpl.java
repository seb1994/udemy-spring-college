package com.formacion.microservicios.app.cursos.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.microservicios.app.cursos.clients.AlumnoFeignClient;
import com.formacion.microservicios.app.cursos.clients.RespuestaFeignClient;
import com.formacion.microservicios.app.cursos.model.entity.Curso;
import com.formacion.microservicios.app.cursos.model.repository.CursoRepository;
import com.formacion.microservicios.commons.services.CommonSrvImpl;
import com.formacion.microservicios.commons.usuarios.model.entity.Alumno;

@Service
public class CursoSrvImpl extends CommonSrvImpl<Curso, CursoRepository> implements CursoSrv {

	@Autowired
	private RespuestaFeignClient respuestaClient;
	
	@Autowired
	private AlumnoFeignClient alumnoClient;
	
	@Override
	@Transactional(readOnly = true)
	public Curso findCursoByAlumnoId(Long id) {		
		return repository.findCursoByAlumnoId(id);
	}

	@Override
	public Iterable<Long> obtenerExamenesRespondidosPorAlumno(Long alumnoId) {
		return respuestaClient.obtenerExamenesRespondidosPorAlumno(alumnoId);
	}

	@Override
	public List<Alumno> obtenerAlumnosPorCurso(List<Long> ids) {
		return (List<Alumno>) alumnoClient.obtenerAlumnosPorCurso(ids);
	}

	@Override
	@Transactional
	public void eliminarCursoAlumnoById(Long id) {
		repository.eliminarCursoAlumnoById(id);
	}

}
