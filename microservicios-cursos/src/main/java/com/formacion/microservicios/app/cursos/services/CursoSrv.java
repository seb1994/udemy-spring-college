package com.formacion.microservicios.app.cursos.services;

import java.util.List;

import com.formacion.microservicios.app.cursos.model.entity.Curso;
import com.formacion.microservicios.commons.services.CommonSrv;
import com.formacion.microservicios.commons.usuarios.model.entity.Alumno;

public interface CursoSrv extends CommonSrv<Curso> {
	
	public Curso findCursoByAlumnoId(Long id);
	
	public Iterable<Long> obtenerExamenesRespondidosPorAlumno(Long alumnoId);
	
	public List<Alumno> obtenerAlumnosPorCurso(List<Long> ids);
	
	public void eliminarCursoAlumnoById(Long id);	
}
