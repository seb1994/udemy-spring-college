package com.formacion.microservicios.app.respuestas.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.microservicios.app.respuestas.model.entity.Respuesta;
import com.formacion.microservicios.app.respuestas.services.RespuestaSrv;

@RestController
public class RespuestaController {
	
	@Autowired
	private RespuestaSrv respuestaSrv;
	
	@PostMapping
	public ResponseEntity<?> crear(@RequestBody Iterable<Respuesta> respuestas){
		respuestas = ((List<Respuesta>) respuestas).stream().map(r ->{
			r.setAlumnoId(r.getAlumno().getId());
			r.setPreguntaId(r.getPregunta().getId());
			return r;
		}).collect(Collectors.toList());		
		Iterable<Respuesta> respuestasDB = respuestaSrv.saveAll(respuestas);
				
		return ResponseEntity.status(HttpStatus.CREATED).body(respuestasDB);
		
	}
	
	@GetMapping("/alumno/{alumnoId}/examen/{examenId}")
	public ResponseEntity<?> obtenerRespuestasPorAlumnoYExamen(@PathVariable Long alumnoId, @PathVariable Long examenId){
		Iterable<Respuesta> respuestas = respuestaSrv.findRespuestaByAlumnoByExamen(alumnoId, examenId);				
		
		return ResponseEntity.ok(respuestas);
		
	}
	
	@GetMapping("/alumno/{alumnoId}/examenes-respondidos")
	public ResponseEntity<?> obtenerExamenesRespondidosPorAlumno(@PathVariable Long alumnoId){
		Iterable<Long> examenesId = respuestaSrv.findExamenesIdsConRespuestasByAlumno(alumnoId);				
		
		return ResponseEntity.ok(examenesId);
		
	}

}
