package com.formacion.microservicios.app.respuestas.services;

import com.formacion.microservicios.app.respuestas.model.entity.Respuesta;

public interface RespuestaSrv {

	public Iterable<Respuesta> saveAll(Iterable<Respuesta> respuestas);
	
	public Iterable<Respuesta> findRespuestaByAlumnoByExamen(Long alumnoId, Long examenId);
	
	public Iterable<Long> findExamenesIdsConRespuestasByAlumno(Long alumnoId);
	
	public Iterable<Respuesta> findByAlumnoId(Long alumnoId);
		
}
